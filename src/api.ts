import { Router, Handler } from 'express';
import Boom from '@hapi/boom';
import fs from 'fs';
import path from 'path';
import passport from 'passport';
import asyncify from 'express-asyncify';
import emailValidator from 'email-validator';
import { Sequelize } from 'sequelize';
import 'reflect-metadata';
// import ctrls from './controllers';
// import { RequestError } from './types/RequestError';
// import handlerWrapper from './middlewares/handlerWrapper';
// import controllersToRoutes from './utils/controllersToRoutes';
// import requester from './utils/requester';
// import MailHandler from './utils/MailHandler';
// import ejsHelper from './utils/ejsLayoutHelper';
// import NotificationHandler from './utils/NotificationHandler';
// import { User } from './models/UserModel';
// import roles from './config/roles';
// import { Order } from './models/OrderModel';

const router = asyncify<Router>(Router());

const templateFolders = path.join(__dirname, '/templates/mails/');

const filesDir: string[] = [];
fs.readdir(templateFolders, (err, files) => {
  files.forEach((file) => {
    const [fileName] = file.split('.');
    filesDir.push(fileName);
  });
});

export default async function api(
  sequelize: Sequelize,
  globalMiddlewares: Handler[] = [],
): Promise<Router> {
//   controllersToRoutes(router, globalMiddlewares, sequelize);

  // WELCOME REQUEST
//   router.get(
//     '/',
//     handlerWrapper(async function action(req, res) {
//     //   const dev = process.env.NODE_ENV === 'development';
//     //   const env = dev ? { env: process.env } : {};
//     //   const routesList = router.stack.map((item) => {
//     //     if (item.route === undefined) {
//     //       return undefined;
//     //     }
//     //     return `${item.route.stack[0].method.toUpperCase()} ${item.route.path}`;
//     //   });
//     //   const routes = dev ? { routes: routesList } : {};
//     //   const controllersName = ctrls.map((controller) => controller.name);
//     //   const controllers = dev ? { controllers: controllersName } : {};
//     //   const models = dev ? { models: Object.keys(this.models) } : {};
//     //   const filesFields: { [key: string]: string[] } = {};

//     //   if (dev) {
//     //     Object.keys(this.models).forEach((model) => {
//     //       if (this.models[model].filesConfig) {
//     //         filesFields[model] = Object.keys(this.models[model].filesConfig);
//     //       }
//     //     });
//     //   }
//     //   const user = dev ? { user: req.user } : {};
//       return res.send({
//         version: '3.0',
//         message: 'Welcome to CRM API. Powered By Alejandro Aguilar',
//         // ...user,
//         // ...controllers,
//         // filesFields: { ...filesFields },
//         // ...models,
//         // ...routes,
//         // ...env,
//         // cache: req.app.cache.getStats(),
//       });
//     }),
//   );
//   router.use(passport.initialize());


//   router.get(['/templates/', '/templates/:template'], async (req, res) => {
//     if (process.env.NODE_ENV !== 'development') {
//       throw Boom.notFound('No se encontró la página');
//     }
//     const { template = 'index' } = req.params;
//     if (!filesDir.includes(template)) {
//       throw Boom.conflict('No se encontró la página');
//     }

//     const order = await Order.findOne({
//       include: [
//         {
//           association: 'OrderProductVariants',
//           required: true,
//           include: [
//             {
//               association: 'ProductVariant',
//               include: [
//                 {
//                   association: 'Product',
//                 },
//               ],
//             },
//           ],
//         },
//       ],
//     });

//     const settings = {
//       filesDir: filesDir.filter((file) => file !== 'index'),
//       order: order.toJSON(),
//     };

//     const render = await ejsHelper(template, settings, null);
//     return res.send(render);
//   });

  router.get('/', async (req, res) => {
    // const email: string = req.body.email || '';
    // if (!emailValidator.validate(email)) {
    //   throw Boom.badRequest('El correo no es válido, intente de nuevo con uno diferente');
    // }

    // const mail = new MailHandler({
    //   email,
    //   subject: 'Mail de prueba',
    //   test: false,
    // });

    // const mailSended = await mail.send('testHealthCheck', { force: false });

    // if (!mailSended) {
    //   throw Boom.serverUnavailable('Ocurrió un error al intentar enviar el correo electrónico');
    // }
    return res.send({ message: 'welcome to API' });
  });

//   router.post('/test/notification', async (req, res) => {
//     // if (process.env.node_env === 'production') {
//     //   // En el entorno de producción
//     //   if (![roles.Admin, roles.Super].includes(req.user.RoleId)) {
//     //     // Sin ser administrador
//     //     throw Boom.forbidden('Solo administradores tienen permiso de realizar esta acción');
//     //   }
//     // }
//     // const { title = 'TEST', body = 'Prueba de notificaciones', fcmToken = 'test' } = req.body;
//     // const notificationHandler = new NotificationHandler({
//     //   title,
//     //   body,
//     //   token: fcmToken,
//     // });
//     // await notificationHandler.send({ force: true });
//     // return res.json({ status: 201 });
//   });



  return router;
}
