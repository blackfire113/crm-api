/* eslint  radix: 0, no-param-reassign: 0, no-prototype-builtins: 0 */
import { AppModel } from '../types/AppModel';
import { PaginateOptions } from '../types/PaginateOptions';

interface PaginateResult<M> {
  pages: number;
  next: boolean;
  prev: boolean;
  limit: number;
  count: number;
  rows: M[];
}

/**
 * Opciones default, en caso de que no se declare el atributo defaultPaginOptions en los modelos o
 * no se reciban las opciones en la petición
 * @type {{limit: number}}
 */
const defaultOptions = {
  limit: 10,
};

/**
 * Quita los elementos null o undefined del objeto de opciones
 * @param opts
 * @returns {*}
 */
function cleanOptions(opts: PaginateOptions): PaginateOptions {
  Object.keys(opts).forEach((key: keyof PaginateOptions) => {
    if (opts[key] === null || opts[key] === undefined) {
      delete opts[key];
    }
  });
  return opts;
}

/**
 * Genera el query y respuesta para la paginación de los Modelos.
 * Este método se agrega como método de clase de todos los modelos
 *
 * @param options
 * @returns {Promise<{pages: number, next: boolean, prev: boolean, limit: any}>}
 */
async function paginate<M>(options: PaginateOptions = {}): Promise<PaginateResult<M>> {
  const query = {
    ...defaultOptions,
    ...this.defaultPaginOptions,
    ...cleanOptions(options),
  };
  const page = query.page ? parseInt(query.page) : 1;
  const limit = query.limit ? parseInt(query.limit) : null;
  const offset = limit * (page - 1);
  const result = await this.findAndCountAll({ ...query, limit, offset });
  const pages = Math.ceil(result.count / limit);
  const next = page < pages;
  const prev = page > 1;

  return { pages, next, prev, limit, ...result };
}

/**
 * Agrega el método paginate, al modelo especificado(target) o a todos los modelos si se envía la
 * instancia de sequelize
 *
 * @param target Model|Sequelize
 * @returns {Promise<void>}
 */
export default async function withPaginate(target: typeof AppModel): Promise<void> {
  target.paginate = paginate;
}
