import bcrypt from 'bcrypt';
import Crypto from 'crypto';
import Boom from '@hapi/boom';

export const generatePassword = (password: string): string => {
  const salt = bcrypt.genSaltSync();
  return bcrypt.hashSync(password, salt);
};

export const checkPassword = (candidate: string, hashedPassword: string): boolean => {
  try {
    return bcrypt.compareSync(candidate, hashedPassword);
  } catch (e) {
    console.error(`Error revisando contraseña: ${e}`);
    throw Boom.notAcceptable(`Error revisando contraseña: ${e}`);
  }
};

export const encryptCipher = (text: string): string => {
  const { API_SECURITY_CYPHER } = process.env;
  // random initialization vector
  const iv = Crypto.randomBytes(16);

  // random salt
  const salt = Crypto.randomBytes(64);

  // derive encryption key: 32 byte key length
  // in assumption the master key is a cryptographic and NOT a password there is no need for
  // a large number of iterations. It may can replaced by HKDF
  // the value of 2145 is randomly chosen!
  const key = Crypto.pbkdf2Sync(API_SECURITY_CYPHER, salt, 2145, 32, 'sha512');

  // AES 256 GCM Mode
  const cipher = Crypto.createCipheriv('aes-256-gcm', key, iv);

  // encrypt the given text
  const encrypted = Buffer.concat([cipher.update(text, 'utf8'), cipher.final()]);

  // extract the auth tag
  const tag = cipher.getAuthTag();

  // generate output
  return Buffer.concat([salt, iv, tag, encrypted]).toString('base64');
};

export const decryptCipher = (cipher: string): string => {
  const { API_SECURITY_CYPHER } = process.env;
  // base64 decoding
  const bData = Buffer.from(cipher, 'base64');

  // convert data to buffers
  const salt = bData.slice(0, 64);
  const iv = bData.slice(64, 80);
  const tag = bData.slice(80, 96);
  const text = bData.slice(96);

  // derive key using; 32 byte key length
  const key = Crypto.pbkdf2Sync(API_SECURITY_CYPHER, salt, 2145, 32, 'sha512');

  // AES 256 GCM Mode
  const decipher = Crypto.createDecipheriv('aes-256-gcm', key, iv);
  decipher.setAuthTag(tag);
  // encrypt the given text
  const decrypted = decipher.update(text) + decipher.final('utf8');

  return decrypted;
};

export const prepareCipher = (param: string): { content?: string; tag?: string; iv?: string } => {
  const regHashC = new RegExp('hashC=(.*)&hashT');
  const regHashT = new RegExp('hashT=(.*)&hashI');
  const regHashI = new RegExp('hashI=(.*)');
  const contentMatch = param.match(regHashC);
  const tagMatch = param.match(regHashT);
  const ivMatch = param.match(regHashI);
  const cipherObject = {
    content: contentMatch && contentMatch.length >= 1 ? contentMatch[1] : null,
    tag: tagMatch && tagMatch.length >= 1 ? tagMatch[1] : null,
    iv: ivMatch && ivMatch.length >= 1 ? ivMatch[1] : null,
  };
  return cipherObject;
};
