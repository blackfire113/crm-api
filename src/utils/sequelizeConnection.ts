import { Sequelize } from 'sequelize';
import db from '../config/db';

export default function sequelizeConnection(): Promise<void | Sequelize> {
  const sequelize = new Sequelize(db);
  if (process.env.NODE_ENV !== 'test') {
    console.info('>> Connecting to database.');
  }
  return sequelize
    .authenticate()
    .then(() => {
      if (process.env.NODE_ENV !== 'test') {
        console.info('>> Connection has been established successfully.');
      }
      return sequelize;
    })
    .catch((err) => {
      console.error('>> Unable to connect to the database:', err);
    });
}
