import { Error } from 'sequelize';
import { GenericModelInstance } from '../models';

export default function noUpdateColumn<I = string>(
  field: keyof I | Partial<keyof I>[],
): (instance: GenericModelInstance) => void {
  return (instance: GenericModelInstance): void => {
    const changed = instance.changed();
    if (Array.isArray(changed) && typeof field === 'string' && changed.includes(field)) {
      throw new Error(`Unable to edit ${field} field`);
    }
    if (Array.isArray(changed) && Array.isArray(field)) {
      field.forEach((item) => {
        if (typeof item === 'string' && changed.includes(item)) {
          throw new Error(`Unable to edit ${item} field`);
        }
      });
    }
  };
}
