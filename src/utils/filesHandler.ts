// Imports the Google Cloud client library
import { Storage, File } from '@google-cloud/storage';
import gcp from '../config/gcp';

const {
  projectId,
  storage: { bucketName },
  credentials,
} = gcp;

async function listFiles(): Promise<File[]> {
  // Creates a client
  const storage = new Storage({ projectId, credentials });

  // Lists files in the bucket
  const [files] = await storage.bucket(bucketName).getFiles();

  return files;
}

async function listFilesByPrefix(prefix: string, delimiter = '/'): Promise<File[]> {
  // [START storage_list_files_with_prefix]

  // Creates a client
  const storage = new Storage({ projectId, credentials });

  /**
   * This can be used to list all blobs in a "folder", e.g. "public/".
   *
   * The delimiter argument can be used to restrict the results to only the
   * "files" in the given "folder". Without the delimiter, the entire tree under
   * the prefix is returned. For example, given these blobs:
   *
   *   /a/1.txt
   *   /a/b/2.txt
   *
   * If you just specify prefix = '/a', you'll get back:
   *
   *   /a/1.txt
   *   /a/b/2.txt
   *
   * However, if you specify prefix='/a' and delimiter='/', you'll get back:
   *
   *   /a/1.txt
   */
  const options: { prefix: string; delimiter?: string } = {
    prefix,
  };

  if (delimiter) {
    options.delimiter = delimiter;
  }

  // Lists files in the bucket, filtered by a prefix
  const [files] = await storage.bucket(bucketName).getFiles(options);

  return files;
}

function get(path: string): File {
  const storage = new Storage({ projectId, credentials });
  return storage.bucket(bucketName).file(path);
}

async function exists(path: string): Promise<boolean> {
  const storage = new Storage({ projectId, credentials });
  const [result] = await storage.bucket(bucketName).file(path).exists();
  return result;
}

async function upload(filePath: string, destination: string): Promise<boolean> {
  // [START storage_upload_file]

  // Creates a client
  const storage = new Storage({ projectId, credentials });

  // Uploads a local file to the bucket
  await storage.bucket(bucketName).upload(filePath, {
    // Support for HTTP requests made with `Accept-Encoding: gzip`
    gzip: true,
    metadata: {
      // Enable long-lived HTTP caching headers
      // Use only if the contents of the file will never change
      // (If the contents will change, use cacheControl: 'no-cache')
      cacheControl: 'public, max-age=31536000',
    },
    destination,
  });
  // console.log(`${filePath} uploaded to ${bucketName} bucket.`);
  // [END storage_upload_file]
  return true;
}

async function download(srcFilename: string, destFilename: string): Promise<boolean> {
  // [START storage_download_file]

  // Creates a client
  const storage = new Storage({ projectId });

  const options = {
    // The path to which the file should be downloaded, e.g. "./file.txt"
    destination: destFilename,
  };

  // Downloads the file
  await storage.bucket(bucketName).file(srcFilename).download(options);

  // console.log(`gs://${bucketName}/${srcFilename} downloaded to ${destFilename}.`);
  return true;
}

async function remove(filename: string): Promise<boolean> {
  // [START storage_delete_file]

  // Creates a client
  const storage = new Storage({ projectId, credentials });

  // Deletes the file from the bucket
  await storage.bucket(bucketName).file(filename).delete();

  // console.log(`gs://${bucketName}/${filename} deleted.`);
  return true;
}

async function move(name: string, srcFilename: string, destFilename: string): Promise<void> {
  // [START storage_move_file]

  // Creates a client
  const storage = new Storage();

  /**
   * TODO(developer): Uncomment the following lines before running the sample.
   */
  // const name = 'Name of a bucket, e.g. my-bucket';
  // const srcFilename = 'File to move, e.g. file.txt';
  // const destFilename = 'Destination for file, e.g. moved.txt';

  // Moves the file within the bucket
  await storage.bucket(name).file(srcFilename).move(destFilename);

  // console.log(`gs://${name}/${srcFilename} moved to gs://${name}/${destFilename}.`);
  // [END storage_move_file]
}

async function copy(
  srcBucketName: string,
  srcFilename: string,
  destBucketName: string,
  destFilename: string,
): Promise<void> {
  // [START storage_copy_file]

  // Creates a client
  const storage = new Storage();

  /**
   * TODO(developer): Uncomment the following lines before running the sample.
   */
  // const srcBucketName = 'Name of the source bucket, e.g. my-bucket';
  // const srcFilename = 'Name of the source file, e.g. file.txt';
  // const destBucketName = 'Name of the destination bucket, e.g. my-other-bucket';
  // const destFilename = 'Destination name of file, e.g. file.txt';

  // Copies the file to the other bucket
  await storage
    .bucket(srcBucketName)
    .file(srcFilename)
    .copy(storage.bucket(destBucketName).file(destFilename));

  // console.log(`gs://${srcBucketName}/${srcFilename} copied to gs://${destBucketName}/${destFilename}.`);
  // [END storage_copy_file]
}

export default {
  upload,
  download,
  remove,
  copy,
  move,
  get,
  exists,
  listFilesByPrefix,
  listFiles,
};
