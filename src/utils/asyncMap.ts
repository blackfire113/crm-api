/* eslint-disable no-await-in-loop */
// async/await map
module.exports = async function asyncMap<T>(
  array: T[],
  cb: (key: T, index: number) => T,
): Promise<T[]> {
  if (!Array.isArray(array)) {
    throw new Error('asyncMap: The first argument must be an Array');
  }
  if (typeof cb !== 'function') {
    throw new Error('asyncMap: The second argument must be a function');
  }
  const clone = array.slice(0);
  for (let index = 0; index < clone.length; index++) {
    clone[index] = await cb(clone[index], index);
  }
  return clone;
};
