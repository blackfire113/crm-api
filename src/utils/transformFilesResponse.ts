import { ValuesType } from 'utility-types';
import { get } from 'lodash';
import { Models } from '../models';
import { FileConfig } from '../types/FileConfig';
import { Record, AttachmentRecord } from './withFiles';
/**
 * Agrega las url del thumbnail, el archivo original y las copias al registro
 *
 * @param config
 * @param record
 * @param model Alias
 * @param field
 * @param id
 * @returns {*}
 */
const addUrls = (
  config: FileConfig,
  record: AttachmentRecord,
  model: string,
  field: string,
  id: number,
): AttachmentRecord => {
  let attachmentRecord = record;
  if (typeof attachmentRecord === 'string') {
    attachmentRecord = { filename: undefined, tmpDir: undefined, url: undefined };
  }
  const { filename, tmpDir, url } = attachmentRecord;
  if (url) return attachmentRecord;

  // Corrige error que envía undefined en las url
  if (!filename) return attachmentRecord;

  // TODO: Manejar PDF's
  const baseUrl = `${process.env.API_UPLOADS_URL}/${model}-${id}/${field}/${tmpDir}`;
  attachmentRecord.url = `${baseUrl}/${filename}`;
  if (config.thumbnail) {
    attachmentRecord.thumbnail = `${baseUrl}/~${filename}`;
  }
  if (typeof config.copies === 'object') {
    Object.keys(config.copies).forEach((copy: string) => {
      attachmentRecord[copy] = `${baseUrl}/${copy}/${filename}`;
    });
  }
  return attachmentRecord;
};

const handleRecord = (Model: ValuesType<typeof Models>, tableRow: Record): Record => {
  const record = tableRow;
  if (!Model) return record;
  const filesConfig = get(Model, 'filesConfig') || {};
  const associations = get(Model, 'associations') || [];
  Object.keys(filesConfig).forEach((field: string) => {
    const config = filesConfig[field];
    if (!config || !record[field]) {
      return;
    }

    const id = record.id as number;
    if (config.limit === 1) {
      const attachmentRecord = record[field] as AttachmentRecord;
      record[field] = addUrls(config, attachmentRecord, Model.name, field, id);
    } else if (Array.isArray(record[field])) {
      const attachmentRecords = record[field] as AttachmentRecord[];
      attachmentRecords.map((file: AttachmentRecord) =>
        addUrls(config, file, Model.name, field, id),
      );
    }
  });

  Object.keys(associations).forEach((association: string) => {
    if (record[association] !== undefined && record[association] !== null) {
      const { name } = associations[association].target;
      const model = get(Model, 'sequelize').models[name] as ValuesType<typeof Models>;
      const records = record[association];
      if (records && !Array.isArray(records)) {
        record[association] = handleRecord(model, records as Record);
      } else {
        // eslint-disable-next-line @typescript-eslint/no-use-before-define,no-use-before-define
        record[association] = transformFilesResponse(model, records as Record) as Record;
      }
    }
  });

  return record;
};

/**
 *
 *
 * @param Model
 * @param records
 * @returns {*}
 */
function transformFilesResponse(
  Model: ValuesType<typeof Models>,
  records: Record | Record[],
): Record | Record[] {
  if (Array.isArray(records)) {
    return records.map((record) => handleRecord(Model, record));
  }
  return handleRecord(Model, records);
}

export default transformFilesResponse;
