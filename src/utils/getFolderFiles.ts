import fs from 'fs';

const getFolderFiles = (path: string, list: string[] = []): string[] => {
  let filesList = list;
  if (fs.existsSync(path)) {
    const files = fs.readdirSync(path);
    files.forEach((file: string) => {
      if (fs.statSync(`${path}/${file}`).isDirectory()) {
        filesList = getFolderFiles(`${path}/${file}`, filesList);
      } else {
        filesList.push(`${path}/${file}`);
      }
    });
  }
  return filesList;
};

export default getFolderFiles;
