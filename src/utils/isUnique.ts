import { BuildOptions, Model } from 'sequelize';
import { GenericModelInstance } from '../models';

import { NextValidation } from '../types/NextValidation';

type ModelStatic = typeof Model & {
  new (values?: object, options?: BuildOptions): Model; // eslint-disable-line
};
export default function isUnique(
  model: ModelStatic,
  value: unknown,
  next: NextValidation,
  errorMessage: string,
  field = 'name',
): void {
  model
    .findOne({
      where: { [field]: value },
      attributes: ['id'],
      paranoid: false,
    })
    .then((found: GenericModelInstance) => {
      const id = this.getDataValue('id');
      if (this.isNewRecord && found) {
        return next(errorMessage);
      }
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      if (found && found.id && id && Number(id) !== found.id) {
        return next(errorMessage);
      }
      return next();
    })
    .catch((err: string) => next(err));
}
