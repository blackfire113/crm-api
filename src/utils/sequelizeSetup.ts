import { Sequelize } from 'sequelize';
import { ValuesType } from 'utility-types';
import { AppModel } from '../types/AppModel';
import sequelizeConnection from './sequelizeConnection';
import models, { Models } from '../models';
import withPaginate from './withPaginate';

function loadModels(sequelize: Sequelize): { [key: string]: ValuesType<typeof Models> } {
  const initializedModels: any = {}; // FIXME: 
  // const initializedModels: { [key: string]: ValuesType<typeof Models> } = {};
  models.forEach((model) => {
    const ModelClass: any = model(sequelize);
    initializedModels[ModelClass.name] = ModelClass;
  });

  Object.keys(initializedModels).forEach((modelName) => {
    if (initializedModels[modelName].associate) {
      initializedModels[modelName].associate(initializedModels as typeof Models);
    }
  });
  return initializedModels;
}

export default async (): Promise<undefined | { sequelize: Sequelize }> => {
  const sequelize = await sequelizeConnection();
  if (sequelize) {
    sequelize.afterDefine((Model: typeof AppModel) => {
      withPaginate(Model);
    });
    const loadedModels = loadModels(sequelize);
    return { sequelize };
  }
  throw new Error('Sequelize: Unable to connect to the database');
};
