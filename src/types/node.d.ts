declare namespace NodeJS {
  export interface ProcessEnv {
    SERVER_PORT: string;
    API_PREFIX: string;
    API_UPLOADS_PATH: string;
    API_UPLOADS_URL: string;
    API_STATICS_PATH: string;
    API_STATICS_URL: string;
    API_DOMAIN: string;
    API_SECURITY_SALT: string;
    API_SECURITY_CYPHER: string;
    API_JWT_SECRET: string;
    API_JWT_EXPIRES_IN: string;
    API_REFRESH_TOKEN_EXPIRES_IN: string;
    API_EMAIL_NOREPLY: string;
    MYSQL_HOST: string;
    MYSQL_PORT: string;
    MYSQL_NAME: string;
    MYSQL_USER: string;
    MYSQL_PASS: string;
    GCP_PROJECT_ID: string;
    GCP_STORAGE_BUCKET_NAME: string;
    SMTP_HOST: string;
    SMTP_PORT: string;
    SMTP_USER: string;
    SMTP_PASS: string;
    TEST_EMAIL: string;
    TEST_PASSWORD: string;
    PUSH_NOTIFICATIONS_ENABLED: string;
    FIREBASE_API_KEY: string;
    FIREBASE_AUTH_DOMAIN: string;
    FIREBASE_DB: string;
    FIREBASE_PROJECT_ID: string;
    FIREBASE_BUCKET: string;
    FIREBASE_MESSAGING_ID: string;
    FIREBASE_APP_ID: string;
    FIREBASE_ENABLED: string;
  }
}
