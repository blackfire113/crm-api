import { Express } from 'express';
import NodeCache from 'node-cache';
import { Sequelize } from 'sequelize';
import { UserInstance } from '../../models/UserModel';
import { CustomerInstance } from '../../models/CustomerModel';
import { StoreInstance } from '../../models/StoreModel';
import { ApiKeyInstance } from '../../models/ApiKeyModel';
import { Locals } from '../Locals';

declare module 'express' {
  interface Application {
    sequelize: Sequelize;
    cache: NodeCache;
  }

  export interface ParamsDictionary {
    [key: string]: string;
  }

  export type ParamsArray = string[];
  export type Params = ParamsDictionary | ParamsArray;

  export interface Request extends Express.Request {
    app: Application;
    user?: UserInstance;
    customer?: CustomerInstance;
    store?: StoreInstance;
    apiKey?: ApiKeyInstance;
    readonly locals?: Locals;
    setLocal: (key: keyof Locals, value: unknown) => void;
  }
}
