import { Express } from 'express';
import NodeCache from 'node-cache';
import { Sequelize } from 'sequelize';
import { UserInstance } from '../../models/UserModel';
import { CustomerInstance } from '../../models/CustomerModel';
import { StoreInstance } from '../../models/StoreModel';
import { ApiKeyInstance } from '../../models/ApiKeyModel';

declare module 'express-serve-static-core' {
  export interface Application {
    sequelize: Sequelize;
    cache: NodeCache;
  }

  export interface Request extends Express.Request {
    app: Application;
    user?: UserInstance;
    customer?: CustomerInstance;
    store?: StoreInstance;
    apiKey?: ApiKeyInstance;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    readonly locals?: { [key: string]: any };
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    setLocal: (key: string, value: any) => void;
  }
}
