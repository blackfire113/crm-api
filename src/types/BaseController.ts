import { Sequelize } from 'sequelize';

// eslint-disable-next-line import/prefer-default-export
export class BaseController {
  // public models: Models;
  public sequelize: Sequelize;

  constructor(sequelize: Sequelize) {
    // this.models = sequelize.models;
    this.sequelize = sequelize;
  }
}
