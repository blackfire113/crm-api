import { BuildOptions, Model } from 'sequelize';
import { PagingOptions } from './PagingOptions';
import { Models } from '../models';

export type ModelStatic<A> = typeof Model & {
  new(values?: A, options?: BuildOptions): A;

  pagingOptions: PagingOptions<A>;

  associate(models: typeof Models): void;
};
