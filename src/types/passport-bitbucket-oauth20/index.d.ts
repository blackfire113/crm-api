declare module 'passport-bitbucket-oauth20' {
  import { Strategy as PassportStrategy } from 'passport';

  export interface StrategyOptions {
    clientID: string;
    clientSecret: string;
    callbackURL: string;
  }
  export class Strategy extends PassportStrategy {
    public constructor(
      options: StrategyOptions,
      verify: <BitbucketProfile, TUser>(
        accessToken: string,
        refreshToken: string,
        profile: BitbucketProfile,
        cb: (err: null | Error, user?: unknown) => void,
      ) => void,
    );
  }
}
