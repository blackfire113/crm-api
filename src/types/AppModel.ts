/* tslint:disable:variable-name */
import { Model } from 'sequelize';
import { Models } from '../models';
import { Attachment } from './Attachment';
import { FileConfig } from './FileConfig';
import { PaginateOptions } from './PaginateOptions';
import { PagingOptions } from './PagingOptions';

export class AppModel<
  // eslint-disable-next-line @typescript-eslint/ban-types
  MA extends {} = any, // eslint-disable-line @typescript-eslint/no-explicit-any
  // eslint-disable-next-line @typescript-eslint/ban-types
  MCA extends {} = any // eslint-disable-line @typescript-eslint/no-explicit-any
> extends Model<MA, MCA> {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  public static pagingOptions: PagingOptions<any>;

  public static associate: (models: typeof Models) => void;

  public static filesConfig?: { [field: string]: FileConfig };

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  public static associations: any;

  public static paginate: <M extends Model>(
    options?: PaginateOptions,
  ) => Promise<{
    rows: M[];
    count: number;
    pages: number;
    next: boolean;
    prev: boolean;
    limit: number;
  }>;

  readonly id?: number;

  // estos campos son necesarios para poder eliminar los archivos que se eliminaron por el
  // cliente y subir los nuevos ya que la función previous de las instancias no esta funcionando
  public __newFiles?: { [tmpKey: string]: Attachment[] };
  public __deletedFiles?: { [tmpKey: string]: Attachment[] };
}

export default AppModel;
