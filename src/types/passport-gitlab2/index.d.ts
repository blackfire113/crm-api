declare module 'passport-gitlab2' {
  import { Strategy as PassportStrategy } from 'passport';

  export interface StrategyOptions {
    clientID: string;
    clientSecret: string;
    callbackURL: string;
  }
  export default class Strategy extends PassportStrategy {
    public constructor(
      options: StrategyOptions,
      verify: <TProfile, TUser>(
        accessToken: string,
        refreshToken: string,
        profile: TProfile,
        cb: (err: null | Error, user?: unknown) => void,
      ) => void,
    );
  }
}
