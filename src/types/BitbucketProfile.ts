/* eslint-disable camelcase */

export default interface Profile {
  id: string;
  displayName: string;
  username: string;
  profileUrl: string;
  provider: string;
  _raw: string;
  _json: {
    username: string;
    display_name: string;
    has_2fa_enabled?: string;
    links: string[];
    nickname: string;
    account_id: string;
    created_on: string;
    is_staff: boolean;
    location: string;
    account_status: string;
    type: string;
    uuid: string;
  };
  _rawEmails: string;
  _jsonEmails: { pagelen: string; values: string[]; page: number; size: number };
  emails: [{ value: string; primary: boolean; verified: boolean }];
}
