import { Moment } from 'moment';
import { UserInstance } from '../models/UserModel';

export interface Session {
  token: string;
  refreshToken: string;
  //   firebaseToken: string;
  accessTokenExpiresIn: string | Date | Moment;
  refreshTokenExpiresIn: string | Date | Moment;
  user: UserInstance;
}
