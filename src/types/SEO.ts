export default interface SEO {
  pageTitle: string;
  pageDescription: string;
}
