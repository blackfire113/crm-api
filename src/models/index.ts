import { Model, ModelCtor } from 'sequelize';
import { NonFunctionKeys } from 'utility-types';
import { AppModel } from '../types/AppModel';
import { FileConfig } from '../types/FileConfig';
// import PermissionRoleModel, { PermissionRole } from './PermissionRoleModel';
// import PermissionModel, { Permission } from './PermissionModel';
// import RoleModel, { Role } from './RoleModel';
import UserModel, { User } from './UserModel';

export const Models = {
//   User,
};

export type SequelizeModels = {
  [model: string]: ModelCtor<AppModel>;
};

export type GenericModel = ModelCtor<Model> &
  User & {
    filesConfig: { [key: string]: FileConfig };
  };

export type GenericModelInstance = InstanceType<GenericModel>;

export type OnlyAttrs<I extends GenericModelInstance> = Omit<
  Pick<I, NonFunctionKeys<I>>,
  'sequelize' | 'isNewRecord'
>;

export default [

];
