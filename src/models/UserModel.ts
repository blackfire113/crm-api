import Boom from '@hapi/boom';
import httpContext from 'express-http-context';
import moment, { duration, Moment } from 'moment';
// import 0 from 'jsonwebtoken';
import ms from 'ms';
// import { v4 as uuid } from 'uuid';
import {
  Sequelize,
  DataTypes,
  BelongsToGetAssociationMixin,
  Optional,
  Association,
  HasManyGetAssociationsMixin,
} from 'sequelize';
import { AppModel } from '../types/AppModel';
import { PagingOptions } from '../types/PagingOptions';
import { Session } from '../types/Session';
import { generatePassword, checkPassword } from '../utils/crypto';
import { FileConfig } from '../types/FileConfig';
import { NextValidation } from '../types/NextValidation';
import isUnique from '../utils/isUnique';
import noUpdateColumn from '../utils/noUpdateColumns';
// import { Role, RoleInstance } from './RoleModel';
// import { Device, DeviceInstance } from './DeviceModel';
// import { Authentication, AuthenticationInstance } from './AuthenticationModel';
// import { Customer, CustomerInstance } from './CustomerModel';
// import { RefreshToken } from './RefreshTokenModel';
// import config from '../config';
// import roles from '../config/roles';

export interface UserAttributes {
  readonly id?: number;
  RoleId: number;
  // Role?: RoleInstance;
  name: string;
  lastName?: string;
  fullName?: string;
  email?: string;
  password?: string;
  phone?: string;
  avatar?: FileConfig;
  enabled: boolean;
  emailConfirmed?: boolean;
  phoneConfirmed?: boolean;
  lastSignIn?: Date | string | Moment;
  createdBy: number;
  updatedBy: number;
  // timestamps!
  readonly createdAt?: Date | string | Moment;
  updatedAt?: Date | string | Moment;
  deletedAt?: Date | string | Moment;
}
interface UserCreationAttrs
  extends Optional<UserAttributes, 'id' | 'createdAt' | 'updatedAt' | 'deletedAt' | 'lastSignIn'> {}

export class User extends AppModel<UserAttributes, UserCreationAttrs> implements UserAttributes {
  public readonly id: UserAttributes['id'];
  public RoleId: UserAttributes['RoleId'];
  public name: UserAttributes['name'];
  public lastName?: UserAttributes['lastName'];
  public fullName?: UserAttributes['fullName'];
  public email?: UserAttributes['email'];
  public password?: UserAttributes['password'];
  public phone?: UserAttributes['phone'];
  public avatar?: UserAttributes['avatar'];
  public enabled: UserAttributes['enabled'];
  public emailConfirmed?: UserAttributes['emailConfirmed'];
  public phoneConfirmed?: UserAttributes['phoneConfirmed'];
  public lastSignIn: UserAttributes['lastSignIn'];
  public createdBy: UserAttributes['createdBy'];
  public updatedBy: UserAttributes['updatedBy'];
  // timestamps!
  public readonly createdAt: UserAttributes['createdAt'];
  public updatedAt: UserAttributes['updatedAt'];
  public deletedAt?: UserAttributes['deletedAt'];
  public static login: (email: string, password: string) => Promise<typeof User.prototype>;
  public static pagingOptions: PagingOptions<UserAttributes>;
  public static associations: {
    // Role: Association<User, Role>;
    // Store: Association<User, Store>;
    // Authentication: Association<User, Authentication>;
  };

  // public static Role: typeof Role;
  // public static Authentication: typeof Authentication;

  public setLastSignIn: (lastSignIn: string | Moment) => ReturnType<typeof User.update>;
  // public getRole!: BelongsToGetAssociationMixin<RoleInstance>;
  public static getSession: (user: typeof User.prototype) => Promise<Session>;
}

export type UserInstance = typeof User.prototype;

export default function setupModel(sequelize: Sequelize): typeof User {
  User.init(
    {
      id: {
        type: DataTypes.INTEGER.UNSIGNED,
        autoIncrement: true,
        primaryKey: true,
      },
      RoleId: {
        type: DataTypes.INTEGER.UNSIGNED,
        allowNull: false,
        references: {
          model: 'Roles',
          key: 'id',
        },
        defaultValue: '',
        validate: {
          notEmpty: { msg: 'El rol de usuario es requerido' },
        },
      },
      name: {
        type: DataTypes.STRING(255),
        allowNull: false,
        defaultValue: '',
        validate: {
          notEmpty: { msg: 'El nombre es requerido' },
        },
      },
      lastName: {
        type: DataTypes.STRING(255),
        allowNull: false,
        defaultValue: '',
      },
      fullName: {
        type: DataTypes.VIRTUAL,
        get(): string {
          return `${this.getDataValue('name')} ${this.getDataValue('lastname')}`;
        },
      },
      email: {
        type: DataTypes.STRING(255),
        validate: {
          isEmail: { msg: 'Escribe email valido' },
          isUnique(value: string, next: NextValidation): void {
            return isUnique.call(
              this,
              User,
              value,
              next,
              'Ya existe un usuario utilizando este email',
              'email',
            );
          },
        },
      },
      password: {
        type: DataTypes.STRING(50),
        allowNull: true,
        set(val): void {
          const password = val || Date.now();
          this.setDataValue('password', generatePassword(password as string));
        },
      },
      phone: {
        type: DataTypes.STRING(20),
        validate: {
          onlyTenNumbers: (value: string, next: (err?: string) => void): void => {
            if (value.toString().length !== 10 || Number.isNaN(Number.parseInt(value, 10))) {
              return next('Escriba un teléfono valido');
            }
            return next();
          },
        },
      },
      avatar: {
        type: DataTypes.JSON,
        allowNull: false,
        defaultValue: {},
      },
      enabled: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: true,
      },
      emailConfirmed: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      phoneConfirmed: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      lastSignIn: {
        type: DataTypes.DATE,
      },
      updatedBy: {
        type: DataTypes.INTEGER.UNSIGNED,
        allowNull: false,
      },
      createdBy: {
        type: DataTypes.INTEGER.UNSIGNED,
        allowNull: false,
        defaultValue: '',
        validate: {
          notEmpty: { msg: 'El id del creador es requerído' },
        },
      },
    },
    { sequelize },
  );

  User.associate = function associate(): void {
    // User.belongsTo(Role);
    // User.belongsTo(Store);
    // User.hasOne(Customer);
    // User.hasMany(Authentication);
    // User.hasMany(Device, { foreignKey: 'createdBy' });
  };

  User.addHook(
    'beforeValidate',
    'avoidUpdateReadOnlyFields',
    noUpdateColumn<UserInstance>(['password', 'deletedAt', 'createdAt']),
  );

  User.addHook('beforeValidate', async (user) => {
    const { id: UserId } = httpContext.get('user') || {};
    if (!user.get('createdBy') && UserId) {
      user.set('createdBy', UserId);
    }
    if (UserId) {
      user.set('updatedBy', UserId);
    }
  });

  User.prototype.toJSON = function toJSON(): UserInstance {
    const result = this.get();
    delete result.password;
    return result;
  };

  // User.login = async (email, password) => {
  //   // const user = await User.findOne({
  //   //   where: { email },
  //   //   include: [{ association: 'Role', attributes: ['id', 'name'] }],
  //   // });
  //   // if (!user) throw Boom.badRequest('El email ingresado no esta registrado');
  //   // if (!user.enabled) throw Boom.badRequest('Usuario inactivo');
  //   // if (!user.emailConfirmed) throw Boom.badRequest('Primero debe confirmar su correo');
  //   // if (!checkPassword(password, user.password)) throw Boom.badRequest('Contraseña incorrecta');
  //   // return user;
  // };

  User.prototype.setLastSignIn = function setLastSignIn(
    lastSignIn,
  ): ReturnType<typeof User.update> {
    return User.update({ lastSignIn }, { where: { id: this.id }, hooks: false });
  };

  User.pagingOptions = {
    limit: 30,
    order: [
      ['createdAt', 'DESC'],
      ['id', 'DESC'],
    ],
  };

  // const filesConfig: { [key: string]: FileConfig } = {
  //   avatar: {
  //     title: 'Foto de perfil',
  //     description: 'Imágenes .jpg, .jpeg y .png',
  //     limit: 1,
  //     allowed: ['jpg', 'jpeg', 'png'],
  //     maxFileSize: 5,
  //     thumbnail: { width: 200, height: 200, fit: 'cover', position: 'entropy' },
  //     resizeOptions: {
  //       width: 1024,
  //       height: 1024,
  //       fit: 'cover',
  //       position: 'entropy',
  //     },
  //     copies: {
  //       big: { width: 500, height: 500, fit: 'cover', position: 'entropy' },
  //       small: { width: 100, height: 100, fit: 'cover', position: 'entropy' },
  //     },
  //     errors: {
  //       maxFileSize: 'No se permiten imágenes de mas de 5 mb',
  //       allowed: 'Solo se permiten imágenes jpg, jpeg y png',
  //     },
  //   },
  // };
  // User.filesConfig = filesConfig;
  // User.getSession = async (user): Promise<Session> => {
  //   const { id, email, phone, createdAt } = user;
  //   const accessTokenExpiresIn = moment().add(duration(ms(config.API_JWT_EXPIRES_IN)));
  //   const token = jwt.sign(
  //     {
  //       id,
  //       type: 'User',
  //       email,
  //       phone,
  //       createdAt,
  //     },
  //     config.API_JWT_SECRET,
  //     {
  //       expiresIn: config.API_JWT_EXPIRES_IN,
  //     },
  //   );
  //   // TODO: Aplicar firebase token
  //   // const firebaseToken = await getFirebaseToken(user.id);
  //   const refreshToken = uuid();
  //   const refreshTokenExpiresIn = moment().add(duration(ms(config.API_REFRESH_TOKEN_EXPIRES_IN)));
  //   await RefreshToken.create({
  //     token: refreshToken,
  //     UserId: id,
  //     expiresIn: refreshTokenExpiresIn,
  //   });
  //   const include = [];
  //   include.push({
  //     attributes: ['id', 'name'],
  //     association: 'Role',
  //   });
  //   if (user.RoleId === roles.Customer) {
  //     include.push({
  //       association: 'Customer',
  //       include: [
  //         {
  //           association: 'DefaultAddress',
  //           include: [
  //             {
  //               association: 'City',
  //               include: [
  //                 {
  //                   association: 'State',
  //                   include: [
  //                     {
  //                       association: 'Country',
  //                     },
  //                   ],
  //                 },
  //               ],
  //             },
  //           ],
  //         },
  //       ],
  //     });
  //   }
  //   if (user.StoreId) {
  //     include.push({
  //       association: 'Store',
  //     });
  //   }

  //   return {
  //     token,
  //     // firebaseToken,
  //     refreshToken,
  //     accessTokenExpiresIn,
  //     refreshTokenExpiresIn,
  //     user: await User.findByPk(user.id, {
  //       include,
  //     }),
  //   };
  // };

  return User;
}
