/* eslint-disable max-classes-per-file, no-global-assign */
import express, { Request, Router } from 'express';
import apicache from 'apicache';
import httpContext from 'express-http-context';
import rateLimit from 'express-rate-limit';
import compression from 'compression';
import expressLayouts from 'express-ejs-layouts';
import cors from 'cors';
import helmet from 'helmet';
import path from 'path';
import NodeCache from 'node-cache';
import asyncify from 'express-asyncify';
import { Sequelize } from 'sequelize';
// import jwtDecode from 'jwt-decode';
import api from './api';
// import { ApiKey } from './models/ApiKeyModel';
// import { Store, StoreInstance } from './models/StoreModel';
// import authenticate from './middlewares/authenticate';
import sequelizeSetup from './utils/sequelizeSetup';
import errorHandler from './middlewares/errorHandler';
import handleFatalError from './middlewares/handleFatalError';
import handleRejection from './middlewares/handleRejection';
import errorRateLimit from './middlewares/errorRateLimit';
import notFoundHandler from './middlewares/notFoundHandler';
// import passportSetup from './utils/passportSetup';
// import envSchema from './config/env.schema';

export default class Server {
  protected app: express.Application;
  protected startDate: number;

  constructor() {
    this.startDate = Date.now();
    this.app = asyncify(express());
    this.config();
  }

  /**
   * Pone las configuraciones de express y sus middlewares por default
   */
  private config(): void {
    const { NODE_ENV, MAX_LIMIT_REQUESTS, API_STATICS_PATH, API_UPLOADS_PATH } = process.env;
    this.app.use(compression());
    this.app.use(cors());

    // This...
    // this.app.use(helmet());

    // ...is equivalent to this:
    NODE_ENV !== 'development' && this.app.use(helmet.contentSecurityPolicy());
    this.app.use(helmet.dnsPrefetchControl());
    // this.app.use(helmet.expectCt());
    this.app.use(helmet.frameguard());
    this.app.use(helmet.hidePoweredBy());
    this.app.use(helmet.hsts());
    this.app.use(helmet.ieNoOpen());
    this.app.use(helmet.noSniff());
    this.app.use(helmet.permittedCrossDomainPolicies());
    this.app.use(helmet.referrerPolicy());
    this.app.use(helmet.xssFilter());
    // Maneja peticiones application/json
    this.app.use(express.json());
    // this.app.use('/statics', express.static(path.resolve(API_STATICS_PATH)));
    // this.app.use('/uploads', express.static(path.resolve(API_UPLOADS_PATH)));
    
    
    // TODO: 
    // if (NODE_ENV === 'development') {
    //   this.app.use(expressLayouts);
    //   this.app.set('layout', path.join(__dirname, '/templates/layouts/default'));
    //   this.app.set('views', path.join(__dirname, '/templates/mails'));
    //   this.app.set('view engine', 'ejs');
    // }

    this.app.use(httpContext.middleware);
    this.app.use(
      rateLimit({
        windowMs: 60 * 1000, // 1 minute
        max: NODE_ENV !== 'test' ? Number(MAX_LIMIT_REQUESTS || 15) : null, // max 15 requests per minute
        headers: true,
        handler: errorRateLimit,
      }),
    );
    
    this.app.use((req, res, next) => {
      if (NODE_ENV !== 'test') {
        console.info('init');
        // console.info(
        //   chalk.yellow.bgBlack(
        //     `${req.method} - ${req.originalUrl} `,
        //   ),
        // );
        console.info('');
      }
      next();
    });
    this.app.use(
      apicache
        .options({
          appendKey: (req: Request) => `${req.method} ${req.path} ${req.headers.authorization}`,
          statusCodes: {
            include: [
              401, 403
            ],
          },
        })
        .middleware('5 minutes'),
    );
    // Middleware que se encarga de inicializar req.locals y req.setLocal
    this.app.use((req, res, next) => {
      if (typeof req.locals === 'undefined') {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        req.locals = {};
      }
      req.setLocal = function setLocal(key: string, value: unknown): void {
        req.locals[key] = value;
      };
      next();
    });
  }

  /**
   * Pone las rutas del API
   * @param sequelize
   */
  private async routes(sequelize: Sequelize): Promise<void> {
    // const router: Router = await api(sequelize, [authenticate]); // TODO: 
    const router: Router = await api(sequelize, []);
    this.app.use(process.env.API_PREFIX, router);
    // Error handlers
    this.app.use(errorHandler);
    this.app.use(notFoundHandler);
  }

  /**
   * Inicia el server http de express
   */
  public serve(): void {
    const port = process.env.SERVER_PORT || 8000;
    this.app.listen(port, () => {
      
      console.info(
        (` >> API ready on http://localhost:${port} `),
        `in ${(Date.now() - this.startDate) / 1000} seconds`,
      );
    });
  }

  /**
   * Se encarga de inicializar la aplicación
   */
  public async initialize(): Promise<express.Application> {
    process.on('uncaughtException', (error) => handleFatalError(error, this.app));
    process.on('unhandledRejection', handleRejection);
    try {
      // TODO: Validate that the environment variables are installed.
    //   const { error } = envSchema.validate(process.env);
    //   if (error) {
    //     throw Error('There are problems with environment variables');
    //   }

      const { sequelize } = await sequelizeSetup();
      this.app.sequelize = sequelize;
      this.app.cache = new NodeCache();
    //   passportSetup();
      await this.routes(sequelize);
      return this.app;
    } catch (error) {
      handleFatalError(error, this.app);
      return null;
    }
  }
}
