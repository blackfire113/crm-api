import Boom from '@hapi/boom';

export default (): Error => {
  throw Boom.tooManyRequests(
    'You have exceeded the limit of requests per minute, please wait a few moments to try again',
  );
};
